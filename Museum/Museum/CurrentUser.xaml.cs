﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;
using System.Data.SqlClient;

namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для CurrentUser.xaml
    /// </summary>
    public partial class CurrentUser : Window
    {
        Employee _user;
        SqlConnection connection;
        bool insertMode;

        public CurrentUser(Employee user, SqlConnection connect)
        {
            insertMode = false;
            _user = new Employee();
            connection = connect;
            _user = user;
            InitializeComponent();
            txtName.Text = _user.Name;
            txtSecondName.Text = _user.SecondName;
            txtSurname.Text = _user.Surname;
            txtPost.Text = _user.Post;
        }

        #region Button Click

        private void btnSave_Click(object sender, RoutedEventArgs e) // Сохранение изменений
        {
            try
            {
                connection.Open();
                _user.Name = txtName.Text;
                _user.SecondName = txtSecondName.Text;
                _user.Surname = txtSurname.Text;
                _user.Post = txtPost.Text;

                if (insertMode)
                {
                    _user.SelfInsert(connection);
                    connection.Close();
                    this.Close();
                }
                else
                {
                    _user.SelfUpdate(connection);
                }
                connection.Close();
                EditModeOff();
            }
            catch (SqlException err)
            {
                connection.Close();
                MessageBox.Show(TriggerMessage.TrimMessage(err.Message));
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) // Отмена
        {
            if (insertMode)
            {
                this.Close();
            }
            else
            {
                EditModeOff();
            }
        }

        #endregion

        #region Menu Item Click

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem Sender = (MenuItem)sender;
            switch (Sender.Name)
            {
                case "Exit":
                    this.Close();
                    break;
                case "Edit":
                    EditModeOn();
                    break;
                case "ShowAll":
                    Users_Window users = new Users_Window(connection);
                    users.ShowDialog();
                    break;
            }
        }

        #endregion

        #region Support Method
       
        public void InsertModeOn() // Открыть форму для вставки 
        {
            insertMode = true;
            EditModeOn();            
        }

        private void EditModeOn() // Открыть форму для изменения 
        {
            txtName.IsEnabled = true;
            txtSecondName.IsEnabled = true;
            txtSurname.IsEnabled = true;
            txtPost.IsEnabled = true;

            btnCancel.Visibility = Visibility.Visible;
            btnSave.Visibility = Visibility.Visible;
        }

        private void EditModeOff() // Закрыть форму для изменения 
        {
            txtName.Text = _user.Name;
            txtSecondName.Text = _user.SecondName;
            txtSurname.Text = _user.Surname;
            txtPost.Text = _user.Post;

            txtName.IsEnabled = false;
            txtSecondName.IsEnabled = false;
            txtSurname.IsEnabled = false;
            txtPost.IsEnabled = false;
            
            btnCancel.Visibility = Visibility.Hidden;
            btnSave.Visibility = Visibility.Hidden;
        }

        #endregion

    }
}
