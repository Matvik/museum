﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum
{
  public static class Input
    {
      public static string InsertString(string header,string txtText)
       {
           string s = null;
           Input_Window input = new Input_Window();
           input.label = header;
           input.Text = txtText;
           input.ShowDialog();
           if (input.isOK) { s = input.Text; }
           return s;
       }

      public static string InsertCombo(string header, string TableName, string ColumnName, SqlConnection connect)
       {
           string s = null;

           List<string> list = new List<string>();
           Input_Window input = new Input_Window();
           input.label = header;
           string query = "select DISTINCT " + ColumnName + " FROM " + TableName;
           SqlCommand command = new SqlCommand(query, connect);
           connect.Open();
           SqlDataReader reader = command.ExecuteReader();
           if (reader.HasRows)
           {
               while (reader.Read())
               {
                   string tmp = "";
                   try
                   { tmp = (string)reader[ColumnName]; }
                   catch (Exception) { }
                   list.Add(tmp);
               }
           }
           reader.Close();
           connect.Close();
           input.ShowCombo(list);
           input.ShowDialog();
           if (input.isOK)
           {
               s = input.Combo;
           }
           return s;
       }

      public static string InsertDate(string header)
       {
           string s = null;

           Input_Window input = new Input_Window();
           input.label = header;
           input.ShowDate();
           input.ShowDialog();
           if (input.isOK)
           {
               s = input.Date;
           }

           return s;
       }
    }
}
