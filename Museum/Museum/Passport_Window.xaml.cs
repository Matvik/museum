﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;
using System.Data;
using System.Collections.ObjectModel;

namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для Passport_Window.xaml
    /// </summary>
    public partial class Passport_Window : Window
    {
        public bool InsertModeOn { get; set; }
        int Employee;
        SqlConnection connection;
        Passport pass;
        public List<Passport> group;
        public bool isNull;

        public Passport_Window(SqlConnection connect, Passport passport, int PersID)
        {
            pass = passport;
            connection = connect;
            Employee = PersID;
            pass.PersID = Employee;
            InitializeComponent();
            FillWindow();
        }

        public Passport_Window(SqlConnection connect, int PersID)
        {
            pass = new Passport();
            connection = connect;
            Employee = PersID;
            pass.PersID = Employee;
            InitializeComponent();
            isNull = true;
            FillWindow();
        }
        public void GroupMode()
        {
            Save.Name = "GroupSave";
        }

        private void FillWindow()
        {
            if (!isNull)
            {
                txt1K.Text = pass.KMuseum;
                txtAnnot.Text = pass.Annotac;
                txtAuthor.Text = pass.Author;
                txtBiblio.Text = pass.Biblio;
                txtConnect.Text = pass.OtherKollection;
                txtDateCreate.Text = pass.DateofCreation.ToShortDateString();
                txtDatePas.Text = pass.DatePassport.ToShortDateString();
                txtDatePX.Text = pass.Date_PX.ToShortDateString();
                txtEmploye.Text = pass.PersID.ToString();
                txtFormat.Text = pass.Format;
                txtIName.Text = pass.ItemName;
                txtInumber.Text = pass.InvNomber;
                txtIzdat.Text = pass.Izdat;
                txtKP.Text = pass.KP;
                txtNazv.Text = pass.Nazvanie;
                txtNoActPX.Text = pass.NoActPX.ToString();
                txtPlaceIzd.Text = pass.PlaceIzdat;
                txtRestor.Text = pass.Restor;
                txtSafety.Text = pass.Safety;
                txtSize.Text = pass.Size;
                txtSverka.Text = pass.Sverka.ToString();
                txtTema.Text = pass.Thematic;
                txtTeth.Text = pass.Teqnique;
                txtTirazh.Text = pass.Tirazh.ToString();
                txtYearIzd.Text = pass.YearIzdat.ToString();
                txtHistory.Text = pass.History;
                txtCoffin.Text = pass.safe.Coffin.ToString();
                txtPolka.Text = pass.safe.Polka.ToString();

                MaterialGrid.ItemsSource = pass.sostav.name_material;
                RaznovidGrid.ItemsSource = pass.raznovid.raznovid;
            }

        }
        private void FillPassport()
        {
            if (txt1K.Text != "") { pass.KMuseum = txt1K.Text; }
            if (txtAnnot.Text != "") { pass.Annotac = txtAnnot.Text; }
            if (txtAuthor.Text != "") { pass.Author = txtAuthor.Text; }
            if (txtBiblio.Text != "") { pass.Biblio = txtBiblio.Text; }
            if (txtConnect.Text != "") { pass.OtherKollection = txtConnect.Text; }
            if (txtDateCreate.Text != "") { pass.DateofCreation = DateTime.Parse(txtDateCreate.Text); }
            if (txtDatePas.Text != "") { pass.DatePassport = DateTime.Parse(txtDatePas.Text); }
            if (txtDatePX.Text != "") { pass.Date_PX = DateTime.Parse(txtDatePX.Text); }
            if (txtEmploye.Text != "") { pass.PersID = Int32.Parse(txtEmploye.Text); }
            if (txtFormat.Text != "") { pass.Format = txtFormat.Text; }
            if (txtIName.Text != "") { pass.ItemName = txtIName.Text; }
            if (txtInumber.Text != "") { pass.InvNomber = txtInumber.Text; }
            if (txtIzdat.Text != "") { pass.Izdat = txtIzdat.Text; }
            if (txtKP.Text != "") { pass.KP = txtKP.Text; }
            if (txtNazv.Text != "") { pass.Nazvanie = txtNazv.Text; }
            if (txtNoActPX.Text != "") { pass.NoActPX = Int32.Parse(txtNoActPX.Text); }
            if (txtPlaceIzd.Text != "") { pass.PlaceIzdat = txtPlaceIzd.Text; }
            if (txtRestor.Text != "") { pass.Restor = txtRestor.Text; }
            if (txtSafety.Text != "") { pass.Safety = txtSafety.Text; }
            if (txtSize.Text != "") { pass.Size = txtSize.Text; }
            if (txtSverka.Text != "") { pass.Sverka = Int32.Parse(txtSverka.Text); }
            if (txtTema.Text != "") { pass.Thematic = txtTema.Text; }
            if (txtTeth.Text != "") { pass.Teqnique = txtTeth.Text; }
            if (txtTirazh.Text != "") { pass.Tirazh = Int32.Parse(txtTirazh.Text); }
            if (txtYearIzd.Text != "") { pass.YearIzdat = Int32.Parse(txtYearIzd.Text); }
            if (txtHistory.Text != "") { pass.History = txtHistory.Text; }
            if (txtPolka.Text != "") { pass.safe.Polka = Int32.Parse(txtPolka.Text); }
            if (txtCoffin.Text != "") { pass.safe.Coffin = Int32.Parse(txtCoffin.Text); }
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Button send = (Button)sender;
            if (send.Name == "GroupSave") { GroupSave(); }
            else
            {
                try
                {
                    FillPassport();
                    connection.Open();

                    SqlTransaction tran = connection.BeginTransaction();

                    if (InsertModeOn)
                    {
                        pass.SelfInsert(connection, tran);
                        // pass.FindSelf(connection);
                    }
                    pass.SelfUpdate(connection, tran);
                    tran.Commit();
                    connection.Close();
                    if (!InsertModeOn)
                    {
                        this.Close();
                    }
                    this.Close();
                }
                catch (SqlException err)
                {
                    connection.Close();
                    MessageBox.Show(TriggerMessage.TrimMessage(err.Message));
                }
            }
        }

        private void GroupSave()
        {
            try
            {
                connection.Open();

                foreach (Passport p in group)
                {
                    pass = p;
                    FillPassport();
                    pass.SelfUpdate(connection);
                }

                connection.Close();
                this.Close();
            }
            catch (SqlException err)
            {
                connection.Close();
                MessageBox.Show(err.Message);
            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void menu_click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            switch (item.Name)
            {
                case"mPrintPass":
                    List_Windows pas = new List_Windows(connection, "select * from Научные_паспорта where КП='" + txtKP.Text+"'", txtKP.Text,0);
                    pas.isVertical = true;
                    pas.ShowDialog();
                    break;
                case"mPrintKard":
                    List_Windows kard = new List_Windows(connection, "select * from Карточки where КП='" + txtKP.Text+"'", txtKP.Text,0);
                    kard.isVertical = true;
                    kard.ShowDialog();
                    break;
                case "mSaveClose":
                    Save_Click(new Button(), e);
                    this.Close();
                    break;
                case "mSaveNewTemplate":                                
                    Save_Click(new Button(), e);
                    InsertModeOn = true;
                    break;
                case "mSaveNewEmpty":                    
                    Save_Click(new Button(), e);
                    InsertModeOn = true;
                    pass = new Passport();
                    pass.PersID = Employee;
                    FillWindow();                    
                    break;
                case "mExit":
                    this.Close();
                    break;
            }
        }
    }
}
