﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;

namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для Users_Window.xaml
    /// </summary>

    public partial class Users_Window : Window
    { 
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand Command;
        DataTable table;

        public Users_Window(SqlConnection sqlconnect)
        {
            connection = sqlconnect;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid("Select * from Сотрудники_ФИО");
        }

        #region Menu Item Click

        private void Print_Click(object sender, RoutedEventArgs e) //Печать списка сотрудников 
        {
            table.TableName = "Сотрудники";
            printer1 prt = new printer1(table, false);
        }

        private void Close_Click(object sender, RoutedEventArgs e) //Закрыть окно 
        {
            this.Close();
        }

        #endregion

        #region Button Click

        private void btnDelete_Click(object sender, RoutedEventArgs e) //Удалить сотрудника
        {
            Employee user = new Employee(SelectedID());
            try
            {
                connection.Open();
                user.SelfDelete(connection);
                connection.Close();
            }
            catch (SqlException err)
            {
                connection.Close();
                MessageBox.Show(err.Message);
            }

            UpdateTable();
        }
        
        private void btnEdit_Click(object sender, RoutedEventArgs e) //Изменить данные о сотруднике 
        {
            Employee user = new Employee(SelectedID());
            try
            {
                connection.Open();
                user.FindSelf(connection);
                connection.Close();
                CurrentUser ViewUser = new CurrentUser(user, connection);
                ViewUser.ShowDialog();
            }
            catch (SqlException err)
            {
                connection.Close();
                MessageBox.Show(err.Message);
            }
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e) //Добавить сотрудника 
        {
            Employee user = new Employee();
            CurrentUser ViewUser = new CurrentUser(user, connection);
            ViewUser.InsertModeOn();
            ViewUser.ShowDialog();
            UpdateTable();
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e) //Открыть данные о сотурдника 
        {
            string query = "EXEC Введено_сотрудником @id = " + SelectedID().ToString();
            string TableName = "Введенные сотрудником " + SelectedID().ToString();
            List_Windows spicok = new List_Windows(connection, query, TableName,SelectedID());
            spicok.ShowDialog();
        }

        #endregion

        #region DataGrid Method

        private void FillDataGrid(string Query) //Заполнение таблицы 
        {
            Command = new SqlCommand(Query, connection);
            adapter = new SqlDataAdapter(Command);
            UpdateTable();
        }

        private void UpdateTable() //Обновление таблицы 
        {
            try
            {
                table = new DataTable();
                adapter.Fill(table);
                EmployeeGrid.ItemsSource = table.DefaultView;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void EmployeeGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) //Изменение курсора 
        {
            if (EmployeeGrid.SelectedItem != null)
            {
                btnDelete.IsEnabled = true;
                btnEdit.IsEnabled = true;
                btnSpOpen.IsEnabled = true;
            }
            else
            {
                btnDelete.IsEnabled = false;
                btnEdit.IsEnabled = false;
                btnSpOpen.IsEnabled = false;
            }
        }

        private int SelectedID() // Возвращает ID выделенной в DataGrid строке 
        {
            if (EmployeeGrid.SelectedItem != null)
            {
                DataRowView row = (DataRowView)EmployeeGrid.SelectedItem;
                return (int)row.Row["№"];
            }
            return 0;
        }

        #endregion

    }
}