﻿using Museum.DB_Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Museum
{
    /// <summary>
    /// Interaction logic for List_Windows.xaml
    /// </summary>
    public partial class List_Windows : Window
    {
        SqlConnection connection;
        SqlDataAdapter _Adapter;
        DataTable _DataTable;
        string command;
        string TableName;
        public bool isVertical;
        int userID;

        public List_Windows(SqlConnection connect, string command,string name, int user)
        {
            connection = connect;
            this.command = command;
            TableName = name;
            userID = user;
            InitializeComponent();
        }

        private void FillDataGrid(string Sqlcommand, string tablename)
        {
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(Sqlcommand, connection);
                _Adapter = new SqlDataAdapter(command);
                _DataTable = new DataTable();
                _Adapter.Fill(_DataTable);
                connection.Close();
                _DataTable.TableName = tablename;
                dataGrid.ItemsSource = _DataTable.DefaultView;
            }
            catch (SqlException err)
            {
                connection.Close();
                MessageBox.Show(err.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid(command,TableName);
        }

        private void Menu_Item_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            switch (item.Name)
            {
                case "Exit":
                    this.Close();
                    break;
                case "Print":
                    Print_Click(sender, e);
                    break;
            }
        }

        private void Print_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                printer1 p = new printer1(_DataTable,isVertical);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void dataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passport _passport = new Passport();
            _passport.KP = SelectedKP();

             connection.Open();
             _passport.FindSelf(connection);
             connection.Close();

             Passport_Window _window = new Passport_Window(connection, _passport, userID);
            _window.InsertModeOn = false;
            _window.ShowDialog();
            FillDataGrid(command, TableName);
        }

        private string SelectedKP()
        {
            if (dataGrid.SelectedItem != null)
            {
                DataRowView row = (DataRowView)dataGrid.SelectedItem;
                return row.Row["Номер КП"].ToString();
            }
            return "КП000000";
        }

    }
}
