﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;
using System.Data.SqlClient;
using System.Data;

namespace Museum
{
    /// <summary>
    /// Interaction logic for Sobranie_window.xaml
    /// </summary>
    public partial class Sobranie_window : Window
    {
        SqlConnection connection;
        
        List<Sobranie> sobr = new List<Sobranie>();
        List<Kollection> kol = new List<Kollection>();
        List<Raznovidnost> razn = new List<Raznovidnost>();

        public Sobranie_window(SqlConnection connect)
        {
            connection = connect;
            InitializeComponent();
            FillList();
        }

        private void FillList()
        {
            sobr = new List<Sobranie>();
            kol = new List<Kollection>();
            razn = new List<Raznovidnost>();
            cbSobranie.Items.Clear();
            cbKollect.Items.Clear();
            cbKollect.IsEnabled = false;
            cbRaznovid.Items.Clear();
            cbRaznovid.IsEnabled = false;

            btnAddKoll.IsEnabled = false;
            btnAddRazn.IsEnabled = false;
            btnDelKoll.IsEnabled = false;
            btnDelRazn.IsEnabled = false;
            btnDelSobr.IsEnabled = false;
            btnEditKoll.IsEnabled = false;
            btnEditRazn.IsEnabled = false;
            btnEditSobr.IsEnabled = false;
            btnOpenKoll.IsEnabled = false;
            btnOpenRazn.IsEnabled = false;
            btnOpenSobr.IsEnabled = false;

            connection.Open();
            string query = "Select * from Собрание";
            SqlCommand command = new SqlCommand(query, connection);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Sobranie s = new Sobranie();
                s.ID = (int)reader["Номер_собрания"];
                s.Name = (string)reader["Название_собрания"];
                s.Sverka = (int)reader["Номер_акта_сверки"];
                s.Count = (int)reader["Кол_во_ед_хранения"];
                sobr.Add(s);
            }
            reader.Close();
            foreach (Sobranie s in sobr)
            {
                cbSobranie.Items.Add(s.Name);
            }

            query = "Select * from Коллекции";
            command = new SqlCommand(query, connection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                Kollection k = new Kollection();
                k.ID = (int)reader["Номер_коллекции"];
                k.Name = (string)reader["Имя_коллекции"];
                k.ID_sobranie = (int)reader["Номер_собрания"];
                k.Count = (int)reader["Кол_во_ед_хранения"];
                kol.Add(k);
            }
            reader.Close();
          
            query = "Select * from Разновидности";
            command = new SqlCommand(query, connection);
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                Raznovidnost r = new Raznovidnost();
                r.ID = (int)reader["Номер_разновидности"];
                r.Name = (string)reader["Название_разновидности"];
                r.ID_kollection = (int)reader["Номер_коллекции"];
                r.Count = (int)reader["Кол_во_ед_хранения"];
                r.Expon = (string)reader["Возможность_экспонирования"];
                razn.Add(r);
            }
            reader.Close();
            
            connection.Close();
        }

        private void cbSobranie_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbKollect.IsEnabled = true;
            cbRaznovid.IsEnabled = false;
            btnDelSobr.IsEnabled = true;
            btnEditSobr.IsEnabled = true;
            btnOpenSobr.IsEnabled = true;
            btnAddKoll.IsEnabled = true;

            cbKollect.Items.Clear();
           string s = (string)cbSobranie.SelectedItem;
           
           var index = from i in sobr
                       where i.Name == s
                       select i.ID;            
            foreach (int i in index)
            {
                var kollection = from k in kol
                                 where k.ID_sobranie == i
                                 select k.Name;
                foreach (string k in kollection)
                {
                    cbKollect.Items.Add(k);
                }
            }
        }

        private void cbKollect_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbRaznovid.IsEnabled = true;
            btnOpenKoll.IsEnabled = true;
            btnEditKoll.IsEnabled = true;
            btnDelKoll.IsEnabled = true;
            btnAddRazn.IsEnabled = true;
            
            cbRaznovid.Items.Clear();
            string s = (string)cbKollect.SelectedItem;

            var index = from i in kol
                        where i.Name == s
                        select i.ID;
            foreach (int i in index)
            {
                var kollection = from k in razn
                                 where k.ID_kollection == i
                                 select k.Name;
                foreach (string k in kollection)
                {
                    cbRaznovid.Items.Add(k);
                }
            }
        }

        private void btnAddSobr_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Sobranie s = new Sobranie();
                Input_Window input = new Input_Window();
                input.label = "Введите имя собрания:";
                input.Text = "";
                bool? dialogResult = input.ShowDialog();
                //switch (dialogResult)
                //{
                //    case true:
                //        // User accepted dialog box                    
                //        break;
                //    case false:
                //        // User canceled dialog box
                //        s.Name = input.text;
                //        connection.Open();
                //        s.SelfInsert(connection);
                //        connection.Close();
                //        FillList();
                //        break;
                //    default:
                //        // Indeterminate
                //        break;
                //}
            }
            catch (SqlException err)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                MessageBox.Show(err.Message);
            }
        }

        private void btnDelSobr_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string s = (string)cbSobranie.SelectedItem;
                Sobranie x = new Sobranie();
                x.Name = s;
                connection.Open();
                x.SelfDelete(connection);
                connection.Close();
                FillList();
            }
            catch (SqlException err)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                MessageBox.Show(err.Message);
            }
        }

        private void btnAddKoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Kollection k = new Kollection();
                Input_Window input = new Input_Window();
                input.label = "Введите имя Коллекции:";
                input.Text = "";
                string x = (string)cbSobranie.SelectedItem;

                var index = from i in sobr
                            where i.Name == x
                            select i.ID;
                int Id_sobr = 0;

                foreach (int i in index)
                {
                    Id_sobr = i;
                }

                k.ID_sobranie = Id_sobr;
                bool? dialogResult = input.ShowDialog();
                //switch (dialogResult)
                //{
                //    case true:
                //        // User accepted dialog box                    
                //        break;
                //    case false:
                //        // User canceled dialog box
                //        if (input.OK)
                //        {
                //            k.Name = input.text;
                //            connection.Open();
                //            k.SelfInsert(connection);
                //            connection.Close();
                //            FillList();
                //        }
                //        break;
                //    default:
                //        // Indeterminate
                //        break;
                //}
            }
            catch (SqlException err)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                MessageBox.Show(err.Message);
            }
        }

        private void btnDelKoll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string s = (string)cbKollect.SelectedItem;
                Kollection x = new Kollection();
                x.Name = s;
                connection.Open();
                x.SelfDelete(connection);
                connection.Close();
                FillList();
            }
            catch (SqlException err)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                MessageBox.Show(err.Message);
            }
        }

        private void cbRaznovid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnDelRazn.IsEnabled = true;
            btnEditRazn.IsEnabled = true;
            btnOpenRazn.IsEnabled = true;
        }

        private void btnAddRazn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Raznovidnost r = new Raznovidnost();
                Input_Window input = new Input_Window();
                input.label = "Введите имя Разновидности:";
                input.Text = "";
                string x = (string)cbKollect.SelectedItem;

                var index = from i in kol
                            where i.Name == x
                            select i.ID;
                int Id_kol = 0;

                foreach (int i in index)
                {
                    Id_kol = i;
                }

                r.ID_kollection = Id_kol;
                bool? dialogResult = input.ShowDialog();
                //switch (dialogResult)
                //{
                //    case true:
                //        // User accepted dialog box                    
                //        break;
                //    case false:
                //        // User canceled dialog box
                //        if (input.OK)
                //        {
                //            r.Name = input.text;
                //            connection.Open();
                //            r.SelfInsert(connection);
                //            connection.Close();
                //            FillList();
                //        }
                //        break;
                //    default:
                //        // Indeterminate
                //        break;
                //}
            }
            catch (SqlException err)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                MessageBox.Show(err.Message);
            }
        }

        private void btnDelRazn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string s = (string)cbRaznovid.SelectedItem;
                Raznovidnost x = new Raznovidnost();
                x.Name = s;
                connection.Open();
                x.SelfDelete(connection);
                connection.Close();
                FillList();
            }
            catch (SqlException err)
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                MessageBox.Show(err.Message);
            }
        }

        private void btnEditRazn_Click(object sender, RoutedEventArgs e)
        {
            string s = (string)cbRaznovid.SelectedItem;
            Raznovidnost x = new Raznovidnost();
            Input_Window input = new Input_Window();
            input.Text = s;
            var index = from i in razn
                        where i.Name == s
                        select i.ID;
            int Id_razn = 0;

            foreach (int i in index)
            {
                Id_razn = i;
            }
            x.ID = Id_razn;


            bool? dialogResult = input.ShowDialog();
            //switch (dialogResult)
            //{
            //    case true:
            //        // User accepted dialog box                    
            //        break;
            //    case false:
            //        // User canceled dialog box
            //        if (input.OK)
            //        {
            //            x.Name = input.text;
            //            connection.Open();
            //            x.SelfUpdate(connection);
            //            connection.Close();
            //            FillList();
            //        }
            //        break;
            //    default:
            //        // Indeterminate
            //        break;
            //}

        }

        private void btnEditKoll_Click(object sender, RoutedEventArgs e)
        {
            string s = (string)cbKollect.SelectedItem;
            Kollection x = new Kollection();
            Input_Window input = new Input_Window();
            input.Text = s;
            var index = from i in kol
                        where i.Name == s
                        select i.ID;
            int Id_razn = 0;

            foreach (int i in index)
            {
                Id_razn = i;
            }
            x.ID = Id_razn;


            bool? dialogResult = input.ShowDialog();
            //switch (dialogResult)
            //{
            //    case true:
            //        // User accepted dialog box                    
            //        break;
            //    case false:
            //        // User canceled dialog box
            //        if (input.OK)
            //        {
            //            x.Name = input.text;
            //            connection.Open();
            //            x.SelfUpdate(connection);
            //            connection.Close();
            //            FillList();
            //        }
            //        break;
            //    default:
            //        // Indeterminate
            //        break;
            //}
        }

        private void btnEditSobr_Click(object sender, RoutedEventArgs e)
        {
            string s = (string)cbSobranie.SelectedItem;
            Sobranie x = new Sobranie();
            Input_Window input = new Input_Window();
            input.Text = s;
            var index = from i in sobr
                        where i.Name == s
                        select i.ID;
            int Id_razn = 0;

            foreach (int i in index)
            {
                Id_razn = i;
            }
            x.ID = Id_razn;


            bool? dialogResult = input.ShowDialog();
            //switch (dialogResult)
            //{
            //    case true:
            //        // User accepted dialog box                    
            //        break;
            //    case false:
            //        // User canceled dialog box
            //        if (input.OK)
            //        {
            //            x.Name = input.text;
            //            connection.Open();
            //            x.SelfUpdate(connection);
            //            connection.Close();
            //            FillList();
            //        }
            //        break;
            //    default:
            //        // Indeterminate
            //        break;
            //}
        }
    }
}
