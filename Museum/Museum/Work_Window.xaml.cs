﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;

namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для Work_Window.xaml
    /// Главная рабочая форма приложения
    /// </summary>
    public partial class Work_Window : Window
    {
        SqlConnection _CurrentConnection;
        SqlDataAdapter _Adapter;
        DataTable _DataTable;
        Employee User;

        public Work_Window(string connection, Employee user, string userRole) //Конструктор с параметрами 
        {
            _CurrentConnection = new SqlConnection(connection);
            User = new Employee();
            User = user;
            InitializeComponent();

            switch (userRole)
            {
                case"Посетитель":
                    SetVisitorView();
                    break;
                case "Практикант":
                    SetPracticantView();
                    break;
                case "Сотрудник":
                    SetEmployeeView();
                    break;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblTitle.Content = TimeOfDay() + User.Name + " !";
            FillDataGrid("Select * from Короткий_вывод");
        }

        #region Users View 

        private void SetVisitorView() // Окно посетителя 
        {
            NewPass.Visibility = Visibility.Collapsed;
            TemplatePass.Visibility = Visibility.Collapsed;
            DeletePass.Visibility = Visibility.Collapsed;
            CurUser.Visibility = Visibility.Collapsed;
            spMenu.Visibility = Visibility.Collapsed;
            groupMenu.Visibility = Visibility.Collapsed;
            structMenu.Visibility = Visibility.Collapsed;
            PassFilter.Visibility = Visibility.Collapsed;
            Admin.Visibility = Visibility.Collapsed;
            FilterColumn.Width = new GridLength(0);
        }

        private void SetPracticantView() // Окно практиканта 
        {
            DeletePass.Visibility = Visibility.Collapsed;
            groupMenu.Visibility = Visibility.Collapsed;
            spMenu.Visibility = Visibility.Collapsed;
            Admin.Visibility = Visibility.Collapsed;
        }

        private void SetEmployeeView() // Окно пользователя 
        {
            Admin.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region Menu Event

        private void Menu_Menu_Click(object sender, RoutedEventArgs e) //Готово. Выход, смена пользователя, открыть пользователя
        {
            MenuItem item = (MenuItem)sender;
            Passport passport = new Passport();
            Passport_Window window = new Passport_Window(_CurrentConnection, passport, User.ID);

            switch (item.Name)
            {
                case "Exit":
                    Application.Current.Shutdown();
                    break;
                case "ChangeUser":
                    Enter_window reentry = new Enter_window();
                    reentry.Show();
                    this.Close();
                    break;
                case "CurUser":
                    CurrentUser user_window = new CurrentUser(User, _CurrentConnection);
                    user_window.ShowDialog();
                    break;
            }
            FillDataGrid("Select * from Короткий_вывод");
        }

        private void Menu_Passport_Click(object sender, RoutedEventArgs e) //Осталось случайный паспорт 
        {
            bool _openWindow = false;
            bool _insetWindow = false;
            MenuItem _item = (MenuItem)sender;
            Passport _passport = new Passport();

            switch (_item.Name)
            {
                case "NewPass":
                    _insetWindow = true;
                    _openWindow = true;
                    break;
                case "OpenPass":
                    _passport.KP = SelectedKP();

                    _CurrentConnection.Open();
                    _passport.FindSelf(_CurrentConnection);
                    _CurrentConnection.Close();

                    _insetWindow = false;
                    _openWindow = true;
                    break;
                case "DeletePass":
                    _passport.KP = SelectedKP();

                    _CurrentConnection.Open();
                    _passport.SelfDelete(_CurrentConnection);
                    _CurrentConnection.Close();
                    break;
                case "TemplatePass":
                    _passport.KP = SelectedKP();

                    _CurrentConnection.Open();
                    _passport.FindSelf(_CurrentConnection);
                    _CurrentConnection.Close();

                    _insetWindow = false;
                    _openWindow = true;
                    break;
                case "RandomPass":
                    {
                        //Получение выходного значения из хранимой в SqlCommand

                        /*
                        _CurrentConnection.Open();
                        SqlCommand random = new SqlCommand("exec Случайный_паспорт", _CurrentConnection);
                        _insetWindow = false;
                        _openWindow = true;
                        */

                        break;
                    }
            }

            if (_openWindow)
            {
                Passport_Window _window = new Passport_Window(_CurrentConnection, _passport, User.ID);
                _window.InsertModeOn = _insetWindow;
                _window.ShowDialog();
            }

            FillDataGrid("Select * from Короткий_вывод");
        }

        private void Menu_Struct_click(object sender, RoutedEventArgs e) // Готово. Показ окон других таблиц 
        {
            MenuItem item = (MenuItem)sender;
            switch (item.Name)
            {
                case "structEmployee":
                    Users_Window users = new Users_Window(_CurrentConnection);
                    users.ShowDialog();
                    break;
                case "structSverka":
                    Sverki_Window sverki = new Sverki_Window(_CurrentConnection,User.ID);
                    sverki.ShowDialog();
                    break;
                case "structSobr":
                    Sobranie_window sobranie = new Sobranie_window(_CurrentConnection);
                    sobranie.ShowDialog();
                    break;
                case "structMaterial":
                    Material_Window Material = new Material_Window(_CurrentConnection);
                    Material.Show();
                    break;
            }
        }

        private void Menu_List_Click(object sender, RoutedEventArgs e) // Нет поиска по дате 
        {
            MenuItem item = (MenuItem)sender;
            List_Windows _window;
            string _command = null;
            string _TableName = null;
            bool IsOK = true;
            switch (item.Name)
            {
                case "spRazn":
                    string razn = Input.InsertCombo("Введите название Разновидности", "Разновидности", "Название_разновидности", _CurrentConnection);
                    if (razn == null) { IsOK = false; }
                    _command = "EXEC Поиск_по_Разновидности '" + razn + "'";
                    _TableName = "Разновидность: " + razn;
                    break;
                case "spKollect":
                    string kol = Input.InsertCombo("Введите название Коллекции", "Коллекции", "Имя_коллекции", _CurrentConnection);
                    if (kol == null) { IsOK = false; }
                    _command = "EXEC Поиск_по_Коллекции '" + kol + "'";
                    _TableName = "Коллекция: " + kol;
                    break;
                case "spSelected":
                    string kp1 = Input.InsertString("Введите первый номер КП", "КП000000");
                    string kp2 = Input.InsertString("Введите последний номер КП", "КП000000");
                    if (kp1 == null || kp2 == null) { IsOK = false; }
                    _command = "EXEC Просмотреть_несколько_паспартов '" + kp1 + "','" + kp2 + "'";
                    _TableName = "От " + kp1 + " до " + kp2;
                    break;
                case "spDate":
                    //throw new Exception();
                    break;

                case "spNoSverka":
                    _command = "EXEC Не_прошедшие_сверку";
                    _TableName = "Не прошедшие сверку";
                    break;
                case "spAll":
                    _command = "Select * from Короткий_вывод";
                    _TableName = "Все предметы собрания";
                    break;
            }
            if (IsOK)
            {
                _window = new List_Windows(_CurrentConnection, _command, _TableName,User.ID);
                _window.ShowDialog();
            }
        }

        private void Menu_Group_Click(object sender, RoutedEventArgs e) // Групповое редактирование 
        {
            List<Passport> Group = new List<Passport>();
            string first = Input.InsertString("Введите первый номер набора", "КП000001");
            string last = Input.InsertString("Введите последний номер набора", "КП000002");
            if (first != null && last != null)
            {
                try
                {
                    _CurrentConnection.Open();
                    string query = "EXEC Просмотреть_несколько_паспартов '" + first + "','" + last + "'";
                    SqlCommand command = new SqlCommand(query, _CurrentConnection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string KP = "";
                            if (!reader.IsDBNull(0)) { KP = reader.GetString(0); }
                            Passport pass = new Passport();
                            pass.KP = KP;
                            Group.Add(pass);
                        }
                    }
                    reader.Close();
                    foreach (Passport pass in Group)
                    {
                        pass.FindSelf(_CurrentConnection);
                    }
                    _CurrentConnection.Close();
                    Passport_Window GroupPass = new Passport_Window(_CurrentConnection, User.ID);
                    GroupPass.GroupMode();
                    GroupPass.group = Group;
                    GroupPass.ShowDialog();
                }
                catch (SqlException err)
                {
                    _CurrentConnection.Close();
                    MessageBox.Show(err.Message);
                }
            }
        }

        private void Admin_Function_click(object sender, RoutedEventArgs e) // Функции админа 
        {
            MenuItem item = (MenuItem)sender;
            string _query = "";
            string from = "", to = "";
            bool isOK = true;
            SqlCommand command;
            switch (item.Name)
            {
                case "InfSobr":
                    FillDataGrid("select * from Информация_о_базе");
                    break;
                case "updateRazn":
                    _query = "exec Обновить_данные_разновидность";
                    break;
                case "updateKol":
                    _query = "exec Обновить_данные_коллекция";
                    break;
                case "updateSobr":
                    _query = "exec Обновить_данные_собрания";
                    break;
                case "moveRazn":
                    from = Input.InsertCombo("Откуда переносим?", "Разновидности", "Название_разновидности", _CurrentConnection);
                    to = Input.InsertCombo("Куда переносим?", "Разновидности", "Название_разновидности", _CurrentConnection);
                    if (from == null || to == null || from.Equals(to)) { isOK = false; }
                    _query = "exec Переместить_в_другую_разновидность '" + from + "', '" + to + "'";
                    break;
                case "moveEmployee":
                    from = Input.InsertCombo("Откуда переносим?", "Сотрудники", "Личный_номер", _CurrentConnection);
                    to = Input.InsertCombo("Откуда переносим?", "Сотрудники", "Личный_номер", _CurrentConnection);
                    if (from == null || to == null || from.Equals(to)) { isOK = false; }
                    _query = "exec Передать_предметы_другому " + from + "," + to + "";
                    break;
            }
            if (_query != "" && isOK)
            {
                command = new SqlCommand(_query, _CurrentConnection);
                try
                {
                    _CurrentConnection.Open();
                    command.ExecuteNonQuery();
                    _CurrentConnection.Close();
                }
                catch (SqlException err)
                {
                    _CurrentConnection.Close();
                    MessageBox.Show(err.Message);
                }
            }

        }

        private void Admin_Cursor_click(object sender, RoutedEventArgs e) // Курсоры админа 
        {
            MenuItem item = (MenuItem)sender;
            string _query = "";
            string _TableName = "";
            bool isOK = true;
            switch (item.Name)
            {
                case "Author_search":
                    string author = Input.InsertCombo("Выберите автора", "Научные_паспорта", "Автор", _CurrentConnection);
                    if (author == null) { isOK = false; }
                    _query = "exec Курсор_КП_автор '" + author + "'";
                    _TableName = "Автора: " + author;
                    break;
                case "SingleMat":
                    _query = "exec Курсор_только_один_материал";
                    _TableName = "Только один материал";
                    break;
                case "CountRare":
                    _query = "exec Курсор_кол_во_редких";
                    _TableName = "Редкие";
                    break;
                case "CountNoSverka":
                    _query = "exec Курсор_кол_во_не_прошли_сверку";
                    _TableName = "Не прошли сверку";
                    break;
                case "DBsave":
                    string path = Environment.CurrentDirectory;
                    SqlCommand cmd = new SqlCommand("exec курсор_Резервное_копирование_базы '" + path + "'", _CurrentConnection);
                    _CurrentConnection.Open();
                    cmd.ExecuteNonQuery();
                    _CurrentConnection.Close();
                    break;
            }
            if (_query != "" && isOK)
            {
                List_Windows list = new List_Windows(_CurrentConnection, _query, _TableName,User.ID);
                list.ShowDialog();
            }
        }

        #endregion

        #region DataGrid method

        private void Filter_Checked(object sender, RoutedEventArgs e) //Готово. Фильтры по главной таблице 
        {
            RadioButton radio = (RadioButton)sender;
            switch (radio.Name)
            {
                case "SearchNone":
                    FillDataGrid("Select * from Короткий_вывод");
                    break;
                case "SearchFull":
                    FillDataGrid("EXEC Полные_Научные_паспорта");
                    break;
                case "SearchEmpty":
                    FillDataGrid("EXEC Неполные_Научные_паспорта");
                    break;
                case "SearchLastMonth":
                    FillDataGrid("select * from Предметы_за_прошлый_месяц");
                    break;
                case "SearchCurrentMonth":
                    FillDataGrid("select * from Предметы_за_этот_месяц");
                    break;
                case "SearchCurrentWeek":
                    FillDataGrid("select * from Предметы_за_эту_неделю");
                    break;
                case "SearchNotSave":
                    FillDataGrid("select * from Не_указано_место_хранения");
                    break;
                case "SearchNotMaterial":
                    FillDataGrid("select * from Не_указан_состав");
                    break;
                case "SearchNotSverka":
                    FillDataGrid("select * from Не_прошли_сверку");
                    break;
                case "SearchOldSverka":
                    FillDataGrid("EXEC Не_прошедшие_сверку");
                    break;
                case "SearchEmployee":
                    FillDataGrid("EXEC Введено_сотрудником @id = " + User.ID);
                    break;
            }
        }

        private void datagrid1_SelectionChanged(object sender, SelectionChangedEventArgs e) // Выбор строки DataGrid 
        {
            if (datagrid1.SelectedIndex != -1)
            {
                OpenPass.IsEnabled = true;
                DeletePass.IsEnabled = true;
                TemplatePass.IsEnabled = true;
                btnEdit.Visibility = Visibility.Visible;
                btnTemplate.Visibility = Visibility.Visible;
            }
            else
            {
                OpenPass.IsEnabled = false;
                DeletePass.IsEnabled = false;
                TemplatePass.IsEnabled = false;
                btnEdit.Visibility = Visibility.Collapsed;
                btnTemplate.Visibility = Visibility.Collapsed;
            }
        }

        private void FillDataGrid(string Sqlcommand) // Заполняет DataGrid 
        {
            try
            {
                SqlCommand command = new SqlCommand(Sqlcommand, _CurrentConnection);
                _Adapter = new SqlDataAdapter(command);
                _DataTable = new DataTable();

                _CurrentConnection.Open();
                _Adapter.Fill(_DataTable);
                _CurrentConnection.Close();

                datagrid1.ItemsSource = _DataTable.DefaultView;
            }
            catch (SqlException err)
            {
                _CurrentConnection.Close();
                MessageBox.Show(err.Message);
            }
        }

        private string SelectedKP() // Возвращает КП выделенной в DataGrid строке 
        {
            if (datagrid1.SelectedItem != null)
            {
                DataRowView row = (DataRowView)datagrid1.SelectedItem;
                return row.Row["Номер КП"].ToString();
            }
            return "КП000000";
        }

        private void datagrid1_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passport _passport = new Passport();

            _passport.KP = SelectedKP();

            _CurrentConnection.Open();
            _passport.FindSelf(_CurrentConnection);
            _CurrentConnection.Close();

            Passport_Window _window = new Passport_Window(_CurrentConnection, _passport, User.ID);
            _window.InsertModeOn = false;
            _window.ShowDialog();

            FillDataGrid("Select * from Короткий_вывод");

        }

        #endregion

        #region Support method

        private string TimeOfDay() //Возвращает время суток 
        {
            int hour = DateTime.Now.Hour;
            if (hour < 6)
                return "Доброй ночи, ";
            else
                if(hour<12)
                    return "Доброе утро, ";
                else
                    if(hour<18)
                        return "Добрый день, ";
                    else
                        return "Добрый вечер, ";
        }       
        #endregion

        #region ButtonClick 

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            Passport pass = new Passport();
            Passport_Window _window = new Passport_Window(_CurrentConnection, pass, User.ID);
            _window.InsertModeOn = true;
            _window.ShowDialog();
            if (SearchNone.IsChecked.Value)
            {
                FillDataGrid("Select * from Короткий_вывод");
            }
            else
            { SearchNone.IsChecked = true; }
        }

        #endregion

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Passport _passport = new Passport();
            _passport.KP = SelectedKP();

            _CurrentConnection.Open();
            _passport.FindSelf(_CurrentConnection);
            _CurrentConnection.Close();

            Passport_Window _window = new Passport_Window(_CurrentConnection, _passport, User.ID);
            _window.InsertModeOn = false;
            _window.ShowDialog();
            if (SearchNone.IsChecked.Value)
            {
                FillDataGrid("Select * from Короткий_вывод");
            }
            else
            { SearchNone.IsChecked = true; }
        }

        private void btnTemplate_Click(object sender, RoutedEventArgs e)
        {
            Passport _passport = new Passport();
            _passport.KP = SelectedKP();

            _CurrentConnection.Open();
            _passport.FindSelf(_CurrentConnection);
            _CurrentConnection.Close();
            _passport.KP = "КП000000";
            Passport_Window _window = new Passport_Window(_CurrentConnection, _passport, User.ID);
            _window.InsertModeOn = true;
            _window.ShowDialog();
            if (SearchNone.IsChecked.Value)
            {
                FillDataGrid("Select * from Короткий_вывод");
            }
            else
            { SearchNone.IsChecked = true; }
        }
    }
}