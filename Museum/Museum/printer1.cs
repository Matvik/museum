﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Data = System.Data;

namespace Museum
{
    class printer1
    {
        Data.DataTable table;
        Word._Application app;
        Word.Document doc;
        Object missing = Type.Missing;
        public bool isVertical;

        public printer1(Data.DataTable input,bool vertical)
        {
            isVertical = vertical;
            table = input;
            app = new Word.Application();
            PrintFile();
        }
        private void PrintFile()
        {                       
            string path =Environment.CurrentDirectory+@"\WordTemp\Table.dotx";
            try
            {
                OpenFile(path);
                FindReplace("@@from", table.TableName);
                FindReplace("@@date", DateTime.Today.ToShortDateString());

                createTable(findMe("@@table"));
                

                app.Visible = true;
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        private void createTable (Word.Range range)
        {
            Word.Range rng;
            int row_count = table.Rows.Count;
            int col_count = table.Columns.Count;
            if (!isVertical)
            {


                Word.Table _table = doc.Tables.Add(range, row_count, col_count);
                _table.Rows.Add();
                _table.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleDouble;
                _table.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleDouble;

                for (int i = 1; i <= col_count; i++)
                {
                    rng = _table.Cell(1, i).Range;
                    rng.Text = table.Columns[i - 1].Caption;
                    rng.Font.Size = 14;
                }

                for (int i = 1; i <= row_count; i++)
                {
                    for (int j = 1; j <= col_count; j++)
                    {
                        rng = _table.Cell(i + 1, j).Range;
                        rng.Text = table.Rows[i - 1][j - 1].ToString();
                        rng.Font.Size = 14;
                    }
                }
            }
            else
            {
                Word.Table _table = doc.Tables.Add(range, col_count-1, row_count+1);
                _table.Rows.Add();
                _table.Borders.OutsideLineStyle = Word.WdLineStyle.wdLineStyleDouble;
                _table.Borders.InsideLineStyle = Word.WdLineStyle.wdLineStyleDouble;

                for (int i = 1; i <= col_count; i++)
                {
                    rng = _table.Cell(i, 1).Range;
                    rng.Text = table.Columns[i - 1].Caption;
                    rng.Font.Size = 14;
                }

                for (int i = 1; i <= row_count; i++)
                {
                    for (int j = 1; j <= col_count; j++)
                    {
                        rng = _table.Cell(j, i + 1).Range;
                        rng.Text = table.Rows[i - 1][j - 1].ToString();
                        rng.Font.Size = 14;
                    }
                }
            }

        }

        private void FindReplace (string str_find, string str_replace)
        {
            Word.Find find = app.Selection.Find;
 
            find.Text = str_find; // текст поиска
            find.Replacement.Text = str_replace; // текст замены
 
            find.Execute(FindText: Type.Missing, MatchCase: false, MatchWholeWord: false, MatchWildcards: false,
                        MatchSoundsLike: missing, MatchAllWordForms: false, Forward: true, Wrap: Word.WdFindWrap.wdFindContinue,
                        Format: false, ReplaceWith: missing, Replace: Word.WdReplace.wdReplaceAll);
        }
        private Word.Range findMe (string str_find)
        {
            object s = str_find;
            Word.Range rng = doc.Paragraphs[3].Range;
            rng.Find.Execute(ref s,
    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
    ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
    ref missing, ref missing);
            return rng;

        }
        

        private void OpenFile (string FileName)
        {
            object path = FileName;
            doc = app.Documents.Open(FileName);
            app.Visible = true;
        }

        private void closeFile()
        {

        }
    }
}
