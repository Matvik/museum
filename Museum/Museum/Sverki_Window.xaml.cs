﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;

namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для Sverki_Window.xaml
    /// </summary>
    public partial class Sverki_Window : Window
    {
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand Command;
        DataTable table;
        int userID;

        public Sverki_Window(SqlConnection connect, int UID)
        {
            connection = connect;
            userID = UID;
            InitializeComponent();
            FillDataGrid("select * from Сверки");
        }

        #region Button Click

        private void btnAddNew_Click(object sender, RoutedEventArgs e) // Добавить сверку 
        {
            SverkaTable sverka = new SverkaTable();
            sverka.ID_user = userID;
            Sverka window = new Sverka(sverka, connection);
            window.InsertMode = true;
            window.ShowDialog();
            UpdateTable();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e) // Удалить сверку 
        {
            SverkaTable sverka = new SverkaTable();
            sverka.ID = SelectedID();
            try
            {
                connection.Open();
                sverka.SelfDelete(connection);
                connection.Close();
            }
            catch (Exception err)
            {
                connection.Close();
                MessageBox.Show(TriggerMessage.TrimMessage(err.Message));
            }
            UpdateTable();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e) // Изменить данные сверки 
        {            
            SverkaTable sverka = new SverkaTable();
            sverka.ID = SelectedID();
            try
            {
                connection.Open();
                sverka.FindSelf(connection);
                connection.Close();
                Sverka window = new Sverka(sverka, connection);
                window.ShowDialog();
            }
            catch (Exception err)
            {
                MessageBox.Show(TriggerMessage.TrimMessage(err.Message));
            }
            UpdateTable();
        }

        #endregion

        #region DataGrid Method

        private void FillDataGrid(string Query) //Заполнить таблицу 
        {
            Command = new SqlCommand(Query, connection);
            adapter = new SqlDataAdapter(Command);
            UpdateTable();
        }

        private void UpdateTable() // Обновить таблицу 
        {
            try
            {
                table = new DataTable();
                adapter.Fill(table);
                SverkiGrid.ItemsSource = table.DefaultView;
            }
            catch (Exception err)
            {
                MessageBox.Show(TriggerMessage.TrimMessage(err.Message));
            }
        }

        private void SverkiGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) //Изменение курсора 
        {
            if (SverkiGrid.SelectedItem != null)
            {
                btnDelete.IsEnabled = true;
                btnEdit.IsEnabled = true;
            }
            else
            {
                btnDelete.IsEnabled = false;
                btnEdit.IsEnabled = false;
            }
        }

        private int SelectedID() // Возвращает ID выделенной в DataGrid строке 
        {
            if (SverkiGrid.SelectedItem != null)
            {
                DataRowView row = (DataRowView)SverkiGrid.SelectedItem;
                return (int)row.Row["Номер_акта_сверки"];
            }
            return 0;
        }

        #endregion

        #region Menu Item Click

        private void Close_Click(object sender, RoutedEventArgs e) // Меню закрыть 
        {
            this.Close();
        }

        #endregion

    }
}
