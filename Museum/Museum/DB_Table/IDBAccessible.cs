﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    interface IDBAccessible
    {
        void FindSelf(SqlConnection connection);
        void SelfInsert(SqlConnection connection);
        void SelfDelete(SqlConnection connection);
        void SelfUpdate(SqlConnection connection);
    }
}
