﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    public class Passport:IDBAccessible
    {
        public int Nomer { get; set; }
        public string KP { get; set; }
        public string InvNomber { get; set; }
        public DateTime Date_PX { get; set; }
        public int NoActPX { get; set; }
        public string Author { get; set; }
        public string Izdat { get; set; }
        public string ItemName { get; set; }
        public string Nazvanie { get; set; }
        public string Annotac { get; set; }
        public string PlaceIzdat { get; set; }
        public int YearIzdat { get; set; }
        public DateTime DatePassport { get; set; }
        public int PersID { get; set; }
        public string KMuseum { get; set; }
        public DateTime DateofCreation { get; set; }
        public string Thematic { get; set; }
        public string OtherKollection { get; set; }
        public string Teqnique { get; set; }
        public string Format { get; set; }
        public string Size { get; set; }
        public string Safety { get; set; }
        public string Restor { get; set; }
        public string History { get; set; }
        public string Biblio { get; set; }
        public int Tirazh { get; set; }
        public int Sverka { get; set; }
        public SafePlace safe { get; set; }
        public Sostav sostav { get; set; }
        public Deployment raznovid { get; set; }

        public Passport()
            : base()
        {
            Date_PX = DatePassport = DateofCreation = DateTime.Today;
            safe = new SafePlace();
            sostav = new Sostav();
            raznovid = new Deployment();
            KP = "КП000000";
        }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Научные_паспорта WHERE КП=@KP";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                if (!reader.IsDBNull(0)) { Nomer = reader.GetInt32(0); }
                if (!reader.IsDBNull(1)) { KP = reader.GetString(1); }
                if (!reader.IsDBNull(2)) { InvNomber = reader.GetString(2); }
                if (!reader.IsDBNull(3)) { Date_PX = reader.GetDateTime(3); }
                if (!reader.IsDBNull(4)) { NoActPX = reader.GetInt32(4); }
                if (!reader.IsDBNull(5)) { Author = reader.GetString(5); }
                if (!reader.IsDBNull(6)) { Izdat = reader.GetString(6); }
                if (!reader.IsDBNull(7)) { ItemName = reader.GetString(7); }
                if (!reader.IsDBNull(8)) { Nazvanie = reader.GetString(8); }
                if (!reader.IsDBNull(9)) { Annotac = reader.GetString(9); }
                if (!reader.IsDBNull(10)) { PlaceIzdat = reader.GetString(10); }
                if (!reader.IsDBNull(11)) { YearIzdat = reader.GetInt32(11); }
                if (!reader.IsDBNull(12)) { DatePassport = reader.GetDateTime(12); }
                if (!reader.IsDBNull(13)) { PersID = reader.GetInt32(13); }
                if (!reader.IsDBNull(14)) { KMuseum = reader.GetString(14); }
                if (!reader.IsDBNull(15)) { DateofCreation = reader.GetDateTime(15); }
                if (!reader.IsDBNull(16)) { Thematic = reader.GetString(16); }
                if (!reader.IsDBNull(17)) { OtherKollection = reader.GetString(17); }
                if (!reader.IsDBNull(18)) { Teqnique = reader.GetString(18); }
                if (!reader.IsDBNull(19)) { Format = reader.GetString(19); }
                if (!reader.IsDBNull(20)) { Size = reader.GetString(20); }
                if (!reader.IsDBNull(21)) { Safety = reader.GetString(21); }
                if (!reader.IsDBNull(22)) { Restor = reader.GetString(22); }
                if (!reader.IsDBNull(23)) { History = reader.GetString(23); }
                if (!reader.IsDBNull(24)) { Biblio = reader.GetString(24); }
                if (!reader.IsDBNull(25)) { Tirazh = reader.GetInt32(25); }
                if (!reader.IsDBNull(26)) { Sverka = reader.GetInt32(26); }
            }
            reader.Close();            
            safe.ID = Nomer;
            safe.FindSelf(connection);
            sostav.ID = Nomer;
            sostav.FindSelf(connection);
            raznovid.ID = Nomer;
            raznovid.FindSelf(connection);
        }
        public void FindSelf(SqlConnection connection,SqlTransaction transaction)
        {
            string query = "SELECT * FROM Научные_паспорта WHERE КП=@KP";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;
            command.Transaction = transaction;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader.HasRows)
            {
                if (!reader.IsDBNull(0)) { Nomer = reader.GetInt32(0); }
                if (!reader.IsDBNull(1)) { KP = reader.GetString(1); }
                if (!reader.IsDBNull(2)) { InvNomber = reader.GetString(2); }
                if (!reader.IsDBNull(3)) { Date_PX = reader.GetDateTime(3); }
                if (!reader.IsDBNull(4)) { NoActPX = reader.GetInt32(4); }
                if (!reader.IsDBNull(5)) { Author = reader.GetString(5); }
                if (!reader.IsDBNull(6)) { Izdat = reader.GetString(6); }
                if (!reader.IsDBNull(7)) { ItemName = reader.GetString(7); }
                if (!reader.IsDBNull(8)) { Nazvanie = reader.GetString(8); }
                if (!reader.IsDBNull(9)) { Annotac = reader.GetString(9); }
                if (!reader.IsDBNull(10)) { PlaceIzdat = reader.GetString(10); }
                if (!reader.IsDBNull(11)) { YearIzdat = reader.GetInt32(11); }
                if (!reader.IsDBNull(12)) { DatePassport = reader.GetDateTime(12); }
                if (!reader.IsDBNull(13)) { PersID = reader.GetInt32(13); }
                if (!reader.IsDBNull(14)) { KMuseum = reader.GetString(14); }
                if (!reader.IsDBNull(15)) { DateofCreation = reader.GetDateTime(15); }
                if (!reader.IsDBNull(16)) { Thematic = reader.GetString(16); }
                if (!reader.IsDBNull(17)) { OtherKollection = reader.GetString(17); }
                if (!reader.IsDBNull(18)) { Teqnique = reader.GetString(18); }
                if (!reader.IsDBNull(19)) { Format = reader.GetString(19); }
                if (!reader.IsDBNull(20)) { Size = reader.GetString(20); }
                if (!reader.IsDBNull(21)) { Safety = reader.GetString(21); }
                if (!reader.IsDBNull(22)) { Restor = reader.GetString(22); }
                if (!reader.IsDBNull(23)) { History = reader.GetString(23); }
                if (!reader.IsDBNull(24)) { Biblio = reader.GetString(24); }
                if (!reader.IsDBNull(25)) { Tirazh = reader.GetInt32(25); }
                if (!reader.IsDBNull(26)) { Sverka = reader.GetInt32(26); }
            }
            reader.Close();
            safe.ID = Nomer;
            safe.FindSelf(connection,transaction);
            sostav.ID = Nomer;
            sostav.FindSelf(connection, transaction);
            raznovid.ID = Nomer;
            raznovid.FindSelf(connection, transaction);
        }
        public void SelfInsert(SqlConnection connection)
        {
            string cmd = "EXEC Вставить_научный_паспорт @KP, @Employee";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;
            command.Parameters.Add("@Employee", SqlDbType.Int).Value = PersID;
            command.ExecuteNonQuery();
            FindSelf(connection);
            safe.SelfInsert(connection);
            sostav.SelfInsert(connection);
            raznovid.SelfInsert(connection);
        }
        public void SelfInsert(SqlConnection connection, SqlTransaction transaction)
        {
            string cmd = "EXEC Вставить_научный_паспорт @KP, @Employee";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;
            command.Parameters.Add("@Employee", SqlDbType.Int).Value = PersID;
            command.Transaction = transaction;
            command.ExecuteNonQuery();
            FindSelf(connection, transaction);
            safe.SelfInsert(connection,transaction);
            sostav.SelfInsert(connection, transaction);
            raznovid.SelfInsert(connection, transaction);
        }

        public void SelfDelete(SqlConnection connection)
        {
            string cmd = "DELETE FROM Научные_паспорта WHERE КП = @KP";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;            
            command.ExecuteNonQuery();            
        }

        public void SelfUpdate(SqlConnection connection)
        {
            string query = @"
                    EXECUTE Обновить_Научный_паспорт
                             @KP
                            ,@InvNo
                            ,@DateOnPX
                            ,@NoActPX
                            ,@Author
                            ,@Izdat
                            ,@ItemName
                            ,@Nazvanie
                            ,@Annotac
                            ,@PlaceIzdat
                            ,@YearIzdat
                            ,@DateSostNP
                            ,@1Kmuseum
                            ,@YearOfCreate
                            ,@Tematic
                            ,@ConnecToOther
                            ,@Tecnique
                            ,@Format
                            ,@Razmer
                            ,@Safety
                            ,@Restor     
                            ,@History                       
                            ,@Biblio
                            ,@Tiraj
                            ,@Sverka";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;
            if (InvNomber == "" || InvNomber == null)
            {
                command.Parameters.AddWithValue("@InvNo", DBNull.Value);
            }
            else
            {
                command.Parameters.Add("@InvNo", SqlDbType.VarChar).Value = InvNomber;
            }

            if (Date_PX == null) { command.Parameters.AddWithValue("@DateOnPX", DBNull.Value); }
            else
            { command.Parameters.Add("@DateOnPX", SqlDbType.DateTime).Value = Date_PX; }

            if (NoActPX == 0) { command.Parameters.AddWithValue("@NoActPX", DBNull.Value); }
            else
            { command.Parameters.Add("@NoActPX", SqlDbType.Int).Value = NoActPX; }

            if (Author == null) { command.Parameters.AddWithValue("@Author", DBNull.Value); }
            else
            { command.Parameters.Add("@Author", SqlDbType.VarChar).Value = Author; }

            if (Izdat == null) { command.Parameters.AddWithValue("@Izdat", DBNull.Value); }
            else
            { command.Parameters.Add("@Izdat", SqlDbType.VarChar).Value = Izdat; }

            if (ItemName == null) { command.Parameters.AddWithValue("@ItemName", DBNull.Value); }
            else
            { command.Parameters.Add("@ItemName", SqlDbType.VarChar).Value = ItemName; }

            if (Nazvanie == null) { command.Parameters.AddWithValue("@Nazvanie", DBNull.Value); }
            else
            { command.Parameters.Add("@Nazvanie", SqlDbType.VarChar).Value = Nazvanie; }

            if (Annotac == null) { command.Parameters.AddWithValue("@Annotac", DBNull.Value); }
            else
            { command.Parameters.Add("@Annotac", SqlDbType.Text).Value = Annotac; }

            if (PlaceIzdat == null) { command.Parameters.AddWithValue("@PlaceIzdat", DBNull.Value); }
            else
            { command.Parameters.Add("@PlaceIzdat", SqlDbType.VarChar).Value = PlaceIzdat; }

            if (YearIzdat == 0) { command.Parameters.AddWithValue("@YearIzdat", DBNull.Value); }
            else
            { command.Parameters.Add("@YearIzdat", SqlDbType.Int).Value = YearIzdat; }

            if (DatePassport == null) { command.Parameters.AddWithValue("@DateSostNP", DBNull.Value); }
            else
            { command.Parameters.Add("@DateSostNP", SqlDbType.DateTime).Value = DatePassport; }

            if (KMuseum == null) { command.Parameters.AddWithValue("@1Kmuseum", DBNull.Value); }
            else
            { command.Parameters.Add("@1Kmuseum", SqlDbType.VarChar).Value = KMuseum; }

            if (DateofCreation == null) { command.Parameters.AddWithValue("@YearOfCreate", DBNull.Value); }
            else
            { command.Parameters.Add("@YearOfCreate", SqlDbType.DateTime).Value = DateofCreation; }

            if (Thematic == null) { command.Parameters.AddWithValue("@Tematic", DBNull.Value); }
            else
            { command.Parameters.Add("@Tematic", SqlDbType.VarChar).Value = Thematic; }

            if (OtherKollection == null) { command.Parameters.AddWithValue("@ConnecToOther", DBNull.Value); }
            else
            { command.Parameters.Add("@ConnecToOther", SqlDbType.Text).Value = OtherKollection; }

            if (Teqnique == null) { command.Parameters.AddWithValue("@Tecnique", DBNull.Value); }
            else
            { command.Parameters.Add("@Tecnique", SqlDbType.VarChar).Value = Teqnique; }

            if (Format == null) { command.Parameters.AddWithValue("@Format", DBNull.Value); }
            else
            { command.Parameters.Add("@Format", SqlDbType.VarChar).Value = Format; }

            if (Size == null) { command.Parameters.AddWithValue("@Razmer", DBNull.Value); }
            else
            { command.Parameters.Add("@Razmer", SqlDbType.VarChar).Value = Size; }

            if (Safety == null) { command.Parameters.AddWithValue("@Safety", DBNull.Value); }
            else
            { command.Parameters.Add("@Safety", SqlDbType.VarChar).Value = Safety; }

            if (Restor == null) { command.Parameters.AddWithValue("@Restor", DBNull.Value); }
            else
            { command.Parameters.Add("@Restor", SqlDbType.VarChar).Value = Restor; }

            if (History == null) { command.Parameters.AddWithValue("@History", DBNull.Value); }
            else
            { command.Parameters.Add("@History", SqlDbType.Text).Value = History; }

            if (Biblio == null) { command.Parameters.AddWithValue("@Biblio", DBNull.Value); }
            else
            { command.Parameters.Add("@Biblio", SqlDbType.Text).Value = Biblio; }

            if (Tirazh == 0) { command.Parameters.AddWithValue("@Tiraj", DBNull.Value); }
            else
            { command.Parameters.Add("@Tiraj", SqlDbType.Int).Value = Tirazh; }

            if (Sverka == 0) { command.Parameters.AddWithValue("@Sverka", DBNull.Value); }
            else
            { command.Parameters.Add("@Sverka", SqlDbType.Int).Value = Sverka; }      

            command.ExecuteNonQuery();

            safe.SelfUpdate(connection);
            sostav.SelfUpdate(connection);
            raznovid.SelfUpdate(connection);

        }

        public void SelfUpdate(SqlConnection connection, SqlTransaction transaction)
        {
            string query = @"
                    EXECUTE Обновить_Научный_паспорт
                             @KP
                            ,@InvNo
                            ,@DateOnPX
                            ,@NoActPX
                            ,@Author
                            ,@Izdat
                            ,@ItemName
                            ,@Nazvanie
                            ,@Annotac
                            ,@PlaceIzdat
                            ,@YearIzdat
                            ,@DateSostNP
                            ,@1Kmuseum
                            ,@YearOfCreate
                            ,@Tematic
                            ,@ConnecToOther
                            ,@Tecnique
                            ,@Format
                            ,@Razmer
                            ,@Safety
                            ,@Restor     
                            ,@History                       
                            ,@Biblio
                            ,@Tiraj
                            ,@Sverka";
            SqlCommand command = new SqlCommand(query, connection);
            command.Transaction = transaction;
            command.Parameters.Add("@KP", SqlDbType.VarChar).Value = KP;
            if (InvNomber == "" || InvNomber == null)
            {
                command.Parameters.AddWithValue("@InvNo", DBNull.Value);
            }
            else
            {
                command.Parameters.Add("@InvNo", SqlDbType.VarChar).Value = InvNomber;
            }

            if (Date_PX == null) { command.Parameters.AddWithValue("@DateOnPX", DBNull.Value); }
            else
            { command.Parameters.Add("@DateOnPX", SqlDbType.DateTime).Value = Date_PX; }

            if (NoActPX == 0) { command.Parameters.AddWithValue("@NoActPX", DBNull.Value); }
            else
            { command.Parameters.Add("@NoActPX", SqlDbType.Int).Value = NoActPX; }

            if (Author == null) { command.Parameters.AddWithValue("@Author", DBNull.Value); }
            else
            { command.Parameters.Add("@Author", SqlDbType.VarChar).Value = Author; }

            if (Izdat == null) { command.Parameters.AddWithValue("@Izdat", DBNull.Value); }
            else
            { command.Parameters.Add("@Izdat", SqlDbType.VarChar).Value = Izdat; }

            if (ItemName == null) { command.Parameters.AddWithValue("@ItemName", DBNull.Value); }
            else
            { command.Parameters.Add("@ItemName", SqlDbType.VarChar).Value = ItemName; }

            if (Nazvanie == null) { command.Parameters.AddWithValue("@Nazvanie", DBNull.Value); }
            else
            { command.Parameters.Add("@Nazvanie", SqlDbType.VarChar).Value = Nazvanie; }

            if (Annotac == null) { command.Parameters.AddWithValue("@Annotac", DBNull.Value); }
            else
            { command.Parameters.Add("@Annotac", SqlDbType.Text).Value = Annotac; }

            if (PlaceIzdat == null) { command.Parameters.AddWithValue("@PlaceIzdat", DBNull.Value); }
            else
            { command.Parameters.Add("@PlaceIzdat", SqlDbType.VarChar).Value = PlaceIzdat; }

            if (YearIzdat == 0) { command.Parameters.AddWithValue("@YearIzdat", DBNull.Value); }
            else
            { command.Parameters.Add("@YearIzdat", SqlDbType.Int).Value = YearIzdat; }

            if (DatePassport == null) { command.Parameters.AddWithValue("@DateSostNP", DBNull.Value); }
            else
            { command.Parameters.Add("@DateSostNP", SqlDbType.DateTime).Value = DatePassport; }

            if (KMuseum == null) { command.Parameters.AddWithValue("@1Kmuseum", DBNull.Value); }
            else
            { command.Parameters.Add("@1Kmuseum", SqlDbType.VarChar).Value = KMuseum; }

            if (DateofCreation == null) { command.Parameters.AddWithValue("@YearOfCreate", DBNull.Value); }
            else
            { command.Parameters.Add("@YearOfCreate", SqlDbType.DateTime).Value = DateofCreation; }

            if (Thematic == null) { command.Parameters.AddWithValue("@Tematic", DBNull.Value); }
            else
            { command.Parameters.Add("@Tematic", SqlDbType.VarChar).Value = Thematic; }

            if (OtherKollection == null) { command.Parameters.AddWithValue("@ConnecToOther", DBNull.Value); }
            else
            { command.Parameters.Add("@ConnecToOther", SqlDbType.Text).Value = OtherKollection; }

            if (Teqnique == null) { command.Parameters.AddWithValue("@Tecnique", DBNull.Value); }
            else
            { command.Parameters.Add("@Tecnique", SqlDbType.VarChar).Value = Teqnique; }

            if (Format == null) { command.Parameters.AddWithValue("@Format", DBNull.Value); }
            else
            { command.Parameters.Add("@Format", SqlDbType.VarChar).Value = Format; }

            if (Size == null) { command.Parameters.AddWithValue("@Razmer", DBNull.Value); }
            else
            { command.Parameters.Add("@Razmer", SqlDbType.VarChar).Value = Size; }

            if (Safety == null) { command.Parameters.AddWithValue("@Safety", DBNull.Value); }
            else
            { command.Parameters.Add("@Safety", SqlDbType.VarChar).Value = Safety; }

            if (Restor == null) { command.Parameters.AddWithValue("@Restor", DBNull.Value); }
            else
            { command.Parameters.Add("@Restor", SqlDbType.VarChar).Value = Restor; }

            if (History == null) { command.Parameters.AddWithValue("@History", DBNull.Value); }
            else
            { command.Parameters.Add("@History", SqlDbType.Text).Value = History; }

            if (Biblio == null) { command.Parameters.AddWithValue("@Biblio", DBNull.Value); }
            else
            { command.Parameters.Add("@Biblio", SqlDbType.Text).Value = Biblio; }

            if (Tirazh == 0) { command.Parameters.AddWithValue("@Tiraj", DBNull.Value); }
            else
            { command.Parameters.Add("@Tiraj", SqlDbType.Int).Value = Tirazh; }

            if (Sverka == 0) { command.Parameters.AddWithValue("@Sverka", DBNull.Value); }
            else
            { command.Parameters.Add("@Sverka", SqlDbType.Int).Value = Sverka; }           
            

            command.ExecuteNonQuery();

            safe.SelfUpdate(connection, transaction);
            sostav.SelfUpdate(connection, transaction);
            raznovid.SelfUpdate(connection, transaction);

        }
    }
}
