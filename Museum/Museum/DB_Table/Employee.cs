﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    public class Employee : IDBAccessible
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }
        public string Post { get; set; }
        public Employee():base()
        {
            ID = -1;
        }
        public Employee(int id):base()
        {
            ID = id;
        }
        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Сотрудники WHERE Личный_номер = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);          
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Name = (string)reader["Имя"];
            Surname = (string)reader["Фамилия"];
            SecondName = (string)reader["Отчество"];
            Post = (string)reader["Должность"];
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = @"INSERT INTO [Сотрудники]
                            ([Имя]
                            ,[Фамилия]
                             ,[Отчество]
                            ,[Должность])
                            VALUES
                                 (@name
                                 ,@secondname
                                 ,@surname
                                  ,@post)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.Parameters.AddWithValue("@secondname", SecondName);
            command.Parameters.AddWithValue("@surname", Surname);
            command.Parameters.AddWithValue("@post", Post);
            command.ExecuteNonQuery();

        }

        public void SelfDelete(SqlConnection connection)
        {
            string query = @"delete from [Сотрудники] where Личный_номер = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();
        }

        public void SelfUpdate(SqlConnection connection)
        {
            string query = @"UPDATE Сотрудники
                             SET [Имя] = @name
                                ,[Фамилия] = @secondname
                                ,[Отчество] = @surname
                                ,[Должность] = @post
                             WHERE [Личный_номер] = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Parameters.AddWithValue("@name", Name);
            command.Parameters.AddWithValue("@secondname", SecondName);
            command.Parameters.AddWithValue("@surname", Surname);
            command.Parameters.AddWithValue("@post", Post);
            command.ExecuteNonQuery();
        }
    }
}
