﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    class MaterialTable:IDBAccessible
    {
        public string Name { get; set; }
        public int ID { get; set; }


        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Материалы WHERE ID_материала = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {                    
                    Name = (string)reader["Материал"];
                }
            }
            reader.Close();
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = @"INSERT INTO Материалы
                                                    ([Материал])
                                                    VALUES
                                                    (@material)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@material", Name);
            command.ExecuteNonQuery();
        }

        public void SelfDelete(System.Data.SqlClient.SqlConnection connection)
        {
            string cmd = "DELETE FROM Материалы WHERE ID_материала = @id";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();  
        }

        public void SelfUpdate(System.Data.SqlClient.SqlConnection connection)
        {
            string query = @"
                                        UPDATE Материалы
                                        SET [Материал] = @material
                                        WHERE ID_материала = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@material", Name);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();
        }
    }
}
