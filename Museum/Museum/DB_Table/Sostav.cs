﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    public class Material
    {
        public string Materials { get; set; }
    }

    public class Sostav : IDBAccessible
    {
        public int ID { get; set; }
        public List<Material> name_material { get; set; }

        public Sostav()
            : base()
        {
            name_material = new List<Material>();
        }

        public void FindSelf(System.Data.SqlClient.SqlConnection connection)
        {
            string query = "SELECT * FROM Состав_названия WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();


            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Material m = new Material();
                    m.Materials = (string)reader["Материал"];
                    name_material.Add(m);
                }
            }
            reader.Close();
        }
        public void FindSelf(System.Data.SqlClient.SqlConnection connection, SqlTransaction transacion)
        {
            string query = "SELECT * FROM Состав_названия WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transacion;
            SqlDataReader reader = command.ExecuteReader();


            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Material m = new Material();
                    m.Materials = (string)reader["Материал"];
                    name_material.Add(m);
                }
            }
            reader.Close();
        }
        public void SelfInsert(System.Data.SqlClient.SqlConnection connection, SqlTransaction transacion)
        {
            string query = @"EXEC Добавить_в_состав @material, @id";
            foreach (Material i in name_material)
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@material", i.Materials);
                command.Parameters.AddWithValue("@id", ID);
                command.Transaction = transacion;
                command.ExecuteNonQuery();
            }
        }

        public void SelfInsert(System.Data.SqlClient.SqlConnection connection)
        {
            string query = @"EXEC Добавить_в_состав @material, @id";
            foreach (Material i in name_material)
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@material", i.Materials);
                command.Parameters.AddWithValue("@id", ID);
                command.ExecuteNonQuery();
            }
        }

        public void SelfDelete(System.Data.SqlClient.SqlConnection connection)
        {
            string cmd = "DELETE FROM Состав WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();
        }
        public void SelfDelete(System.Data.SqlClient.SqlConnection connection, SqlTransaction transacion)
        {
            string cmd = "DELETE FROM Состав WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transacion;
            command.ExecuteNonQuery();
        }

        public void SelfUpdate(System.Data.SqlClient.SqlConnection connection, SqlTransaction transacion)
        {
            SelfDelete(connection, transacion);
            SelfInsert(connection, transacion);
        }

        public void SelfUpdate(System.Data.SqlClient.SqlConnection connection)
        {
            SelfDelete(connection);
            SelfInsert(connection);
        }
    }
}
