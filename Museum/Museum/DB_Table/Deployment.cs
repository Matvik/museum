﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
   public  class Raznovidnosty
    {
        public string Raznovid { get; set; }
    }

   public class Deployment:IDBAccessible
    {
        public int ID { get; set; }
        public List<Raznovidnosty> raznovid { get; set; }

        public Deployment(): base()
        {
            raznovid = new List<Raznovidnosty>();
        }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Определение_название WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Raznovidnosty m = new Raznovidnosty();
                    m.Raznovid = (string)reader["Название_разновидности"];
                    raznovid.Add(m);
                }
            }
            reader.Close();
        }

        public void FindSelf(SqlConnection connection, SqlTransaction transaction)
        {
            string query = "SELECT * FROM Определение_название WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transaction;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Raznovidnosty m = new Raznovidnosty();
                    m.Raznovid = (string)reader["Название_разновидности"];
                    raznovid.Add(m);
                }
            }
            reader.Close();
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = @"EXEC Добавить_в_определение @raznovid, @id";
            foreach (Raznovidnosty i in raznovid)
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@raznovid", i.Raznovid);
                command.Parameters.AddWithValue("@id", ID);
                command.ExecuteNonQuery();
            }
        }

        public void SelfInsert(SqlConnection connection, SqlTransaction transaction)
        {
            string query = @"EXEC Добавить_в_определение @raznovid, @id";
            foreach (Raznovidnosty i in raznovid)
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@raznovid", i.Raznovid);
                command.Parameters.AddWithValue("@id", ID);
                command.Transaction = transaction;
                command.ExecuteNonQuery();
            }
        }


        public void SelfDelete(SqlConnection connection)
        {
            string cmd = "DELETE FROM Определение WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();  
        }
        public void SelfDelete(SqlConnection connection, SqlTransaction transaction)
        {
            string cmd = "DELETE FROM Определение WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(cmd, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transaction;
            command.ExecuteNonQuery();
        }
        public void SelfUpdate(SqlConnection connection, SqlTransaction transaction)
        {
            SelfDelete(connection, transaction);
            SelfInsert(connection, transaction);
        }
        public void SelfUpdate(SqlConnection connection)
        {
            SelfDelete(connection);
            SelfInsert(connection);
        }
    }
}
