﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Museum.DB_Table
{
    class Raznovidnost:IDBAccessible
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public int ID_kollection { get; set; }
        public int Count { get; set; }
        public string Expon { get; set; }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Разновидности WHERE Номер_разновидности = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Name = (string)reader["Название_разновидности"];
            ID_kollection = (int)reader["Номер_коллекции"];
            Count = (int)reader["Кол_во_ед_хранения"];
            Expon = (string)reader["Возможность_экспонирования"];
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = @"
            INSERT INTO [Разновидности]
           ([Название_разновидности]
           ,[Кол_во_ед_хранения]
           ,[Возможность_экспонирования]
           ,[Номер_коллекции])
            VALUES
           (@name
           ,0
           ,'Да'
           ,@id_kol)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.Parameters.AddWithValue("@id_kol", ID_kollection);
            command.ExecuteNonQuery();
        }

        public void SelfDelete(SqlConnection connection)
        {
            string query = "Delete from Разновидности where Название_разновидности = @name";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }

        public void SelfUpdate(SqlConnection connection)
        {
            string query = @"UPDATE Разновидности
                             SET [Название_разновидности] = @name                                          
                             WHERE [Номер_разновидности] = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }
    }
}
