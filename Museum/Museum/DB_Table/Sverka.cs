﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    public class SverkaTable
    {
        public DateTime Date { get; set; }
        public int ID { get; set; }
        public int ID_user { get; set; }

        public SverkaTable():base()
        {
            Date = DateTime.Today;
        }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Сверки WHERE Номер_акта_сверки = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Date = (DateTime)reader["Дата_сверки"];
            ID_user = (int)reader["Личный_номер"];
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = @"INSERT INTO [Сверки]
                            ([Дата_сверки]
                            ,[Личный_номер]
                             ,[Номер_акта_сверки])
                            VALUES
                                 (@date
                                 ,@persId
                                 ,@id)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@date", Date);
            command.Parameters.AddWithValue("@persId", ID_user);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();
        }

        public void SelfDelete(SqlConnection connection)
        {
            string query = @"delete from [Сверки] where Номер_акта_сверки = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();
        }

        public void SelfUpdate(SqlConnection connection)
        {
            string query = @"UPDATE Сверки
                             SET [Дата_сверки] = @date
                                ,[Личный_номер] = @persId
                             WHERE [Номер_акта_сверки] = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Parameters.AddWithValue("@date", Date);
            command.Parameters.AddWithValue("@persId", ID_user);
            command.ExecuteNonQuery();
        }
    }
}
