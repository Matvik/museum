﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Museum.DB_Table
{
    class Kollection:IDBAccessible
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public int ID_sobranie { get; set; }
        public int Count { get; set; }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Коллекции WHERE Номер_коллекции = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Name = (string)reader["Имя_коллекции"];
            ID_sobranie = (int)reader["Номер_собрания"];
            Count = (int)reader["Кол_во_ед_хранения"];
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = @"
            INSERT INTO [Коллекции]
           ([Имя_коллекции]
           ,[Кол_во_ед_хранения]
           ,[Номер_собрания])
            VALUES
           (@name
           ,0
           ,@id_sobr)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.Parameters.AddWithValue("@id_sobr", ID_sobranie);
            command.ExecuteNonQuery();
        }

        public void SelfDelete(SqlConnection connection)
        {
            string query = "Delete from Коллекции where Имя_коллекции = @name";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }

        public void SelfUpdate(SqlConnection connection)
        {
            string query = @"UPDATE Коллекции
                             SET [Имя_коллекции] = @name                                                           
                             WHERE [Номер_коллекции] = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }
    }
}
