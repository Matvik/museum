﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
    class Sobranie:IDBAccessible
    {
        public string Name { get; set; }
        public int ID { get; set; }
        public int Sverka { get; set; }
        public int Count { get; set; }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Собрание WHERE Номер_собрания = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            Name = (string)reader["Название_собрания"];
            Sverka = (int)reader["Номер_акта_сверки"];
            Count = (int)reader["Кол_во_ед_хранения"];
        }

        public void SelfInsert(SqlConnection connection)
        {
            string query = "Exec Добавить_собрание @name";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }

        public void SelfDelete(SqlConnection connection)
        {
            string query = "Delete from Собрание where Название_собрания = @name";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }

        public void SelfUpdate(SqlConnection connection)
        {
            string query = @"UPDATE Собрание
                             SET [Название_собрания] = @name                                             
                             WHERE [Номер_собрания] = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Parameters.AddWithValue("@name", Name);
            command.ExecuteNonQuery();
        }
    }
}
