﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Museum.DB_Table
{
   public class SafePlace:IDBAccessible
    {
        public int Coffin { get; set; }
        public int Polka { get; set; }
        public int ID { get; set; }

       public SafePlace ():base()
        {
            Coffin = 1;
            Polka = 1;
        }

        public void FindSelf(SqlConnection connection)
        {
            string query = "SELECT * FROM Места_хранения WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            SqlDataReader reader = command.ExecuteReader();
            
            reader.Read();
            if (reader.HasRows)
            {
                Coffin = (int)reader["Номер_шкафа"];
                Polka = (int)reader["Номер_полки"];
            }
            reader.Close();
        }
        public void FindSelf(SqlConnection connection, SqlTransaction transaction)
        {
            string query = "SELECT * FROM Места_хранения WHERE Номер_паспорта = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transaction;
            SqlDataReader reader = command.ExecuteReader();

            reader.Read();
            if (reader.HasRows)
            {
                Coffin = (int)reader["Номер_шкафа"];
                Polka = (int)reader["Номер_полки"];
            }
            reader.Close();
        }

        public void SelfInsert(System.Data.SqlClient.SqlConnection connection)
        {
            string query = @"
            INSERT INTO Места_хранения
           ([Номер_шкафа]
           ,[Номер_полки]
           ,[Номер_паспорта])
            VALUES
           (@coffin
           ,@polka
           ,@id)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@coffin", Coffin);
            command.Parameters.AddWithValue("@polka", Polka);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();
        }

        public void SelfInsert(System.Data.SqlClient.SqlConnection connection, SqlTransaction transaction)
        {
            string query = @"
            INSERT INTO Места_хранения
           ([Номер_шкафа]
           ,[Номер_полки]
           ,[Номер_паспорта])
            VALUES
           (@coffin
           ,@polka
           ,@id)";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@coffin", Coffin);
            command.Parameters.AddWithValue("@polka", Polka);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transaction;
            command.ExecuteNonQuery();
        }

        public void SelfDelete(System.Data.SqlClient.SqlConnection connection)
        {
            throw new NotImplementedException();
        }

        public void SelfUpdate(System.Data.SqlClient.SqlConnection connection, SqlTransaction transaction)
        {
            string query = @"
                                        UPDATE Места_хранения
                                        SET [Номер_шкафа] = @coffin
                                        ,[Номер_полки] = @polka      
                                        WHERE Номер_паспорта = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@coffin", Coffin);
            command.Parameters.AddWithValue("@polka", Polka);
            command.Parameters.AddWithValue("@id", ID);
            command.Transaction = transaction;
            command.ExecuteNonQuery();

        }

        public void SelfUpdate(System.Data.SqlClient.SqlConnection connection)
        {
            string query = @"
                                        UPDATE Места_хранения
                                        SET [Номер_шкафа] = @coffin
                                        ,[Номер_полки] = @polka      
                                        WHERE Номер_паспорта = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@coffin", Coffin);
            command.Parameters.AddWithValue("@polka", Polka);
            command.Parameters.AddWithValue("@id", ID);
            command.ExecuteNonQuery();

        }
    }
}
