﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Museum
{
    /// <summary>
    /// Interaction logic for Input_Window.xaml
    /// </summary>
    public partial class Input_Window : Window
    {
        public string Text { get { return txtInput.Text; } set { txtInput.Text = value; } }
        public string Combo
        {
            get
            {
                if(cbInput.SelectedIndex == -1)
                { return null; }
                return cbInput.SelectedItem.ToString();
            }
        }
        public string Date { get { return dpInput.Text; } }
        public string label { set { lblInput.Content = value; } }
        public bool isOK;
        public Input_Window()
        {
            isOK = true;
            InitializeComponent();
            txtInput.Focus();
        }
        public void ShowCombo(List<string> list)
        {
            cbInput.ItemsSource = list;

            cbInput.Visibility = Visibility.Visible;
            txtInput.Visibility = Visibility.Collapsed;
            dpInput.Visibility = Visibility.Collapsed;
        }
        public void ShowDate()
        {
            dpInput.DisplayDate = DateTime.Today;

            cbInput.Visibility = Visibility.Collapsed;
            txtInput.Visibility = Visibility.Collapsed;
            dpInput.Visibility = Visibility.Visible;
        }
        public void ShowText(string text)
        {
            txtInput.Text = text;
            cbInput.Visibility = Visibility.Collapsed;
            txtInput.Visibility = Visibility.Visible;
            dpInput.Visibility = Visibility.Collapsed;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            isOK = false;
            this.Close();
        }

        private void Input_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Close();
            }
            else
                if(e.Key ==Key.Escape)
                {
                    isOK = false;
                    this.Close();
                }
        }        
    }
}
