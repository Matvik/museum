﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;
using System.Data.SqlClient;

namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для Sverka.xaml
    /// </summary>
    public partial class Sverka : Window
    {
        SverkaTable sverka;
        SqlConnection connection;
        bool isInsert;
        public Sverka(SverkaTable current, SqlConnection connect)
        {
            sverka = current;
            connection = connect;
            isInsert = false;
            InitializeComponent();
            dpDate.DisplayDate = DateTime.Today;
        }

        public bool InsertMode
        {
            set{isInsert = value;}
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dpDate.DisplayDate = sverka.Date;
            txtnumber.Text = sverka.ID_user.ToString();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            sverka.ID = Int32.Parse(txtnumber.Text);
            sverka.Date = dpDate.DisplayDate;
            connection.Open();
            if(isInsert)
            {
                sverka.SelfInsert(connection);
            }
            else
            {
                sverka.SelfUpdate(connection);
            }
            connection.Close();
            this.Close();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            switch (item.Name)
            {
                case "Exit":
                    this.Close();
                    break;
            }
        }
    }
}
