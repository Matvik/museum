﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Museum.DB_Table;
namespace Museum
{
    /// <summary>
    /// Interaction logic for Material_Window.xaml
    /// </summary>
    public partial class Material_Window : Window
    {
        SqlConnection connection;
        SqlDataAdapter adapter;
        SqlCommand command;
        DataTable table;

        public Material_Window(SqlConnection connect)
        {
            connection = connect;
            InitializeComponent();
        }

        private void FillDataGrid(string query)
        {
            command = new SqlCommand(query, connection);
            adapter = new SqlDataAdapter(command);
            table = new DataTable();
            adapter.Fill(table);
            MaterialGrid.ItemsSource = table.DefaultView;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FillDataGrid("Select * from Материалы");
        }

        private void AddNew_Click(object sender, RoutedEventArgs e)
        {
            MaterialTable material = new MaterialTable();
            Input_Window input = new Input_Window();
            input.label = "Введите название материала";
            input.Text = "";
            bool? dialogResult = input.ShowDialog();
            switch (dialogResult)
            {
                case true:
                    // User accepted dialog box                    
                    break;
                case false:
                    // User canceled dialog box
                    material.Name = input.Text;
                    connection.Open();
                    material.SelfInsert(connection);
                    connection.Close();
                    FillDataGrid("Select * from Материалы");
                    break;
                default:
                    // Indeterminate
                    break;
            }
        }

        private void DeleteSelected_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row = (DataRowView)MaterialGrid.SelectedItem;
            MaterialTable material = new MaterialTable();
            material.ID = (int)row.Row[0];
            connection.Open();
            material.SelfDelete(connection);
            connection.Close();
            FillDataGrid("Select * from Материалы");
        }

        private void EditSelected_Click(object sender, RoutedEventArgs e)
        {
            DataRowView row = (DataRowView)MaterialGrid.SelectedItem;
            MaterialTable material = new MaterialTable();
            material.ID = (int)row.Row[0];
            connection.Open();
            material.FindSelf(connection);
            connection.Close();
            Input_Window input = new Input_Window();
            input.Text = material.Name;

            bool? dialogResult = input.ShowDialog();
            switch (dialogResult)
            {
                case true:
                    // User accepted dialog box                    
                    break;
                case false:
                    // User canceled dialog box
                    material.Name = input.Text;
                    connection.Open();
                    material.SelfUpdate(connection);
                    connection.Close();
                    FillDataGrid("Select * from Материалы");
                    break;
                default:
                    // Indeterminate
                    break;
            }
        }

        private void MaterialGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(MaterialGrid.SelectedItem != null)
            {
                DeleteSelected.IsEnabled = true;
                EditSelected.IsEnabled = true;
            }
            else
            {
                DeleteSelected.IsEnabled = false;
                EditSelected.IsEnabled = false;
            }
        }
    }
}
