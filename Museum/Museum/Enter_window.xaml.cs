﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Configuration;
using Museum.DB_Table;
namespace Museum
{
    /// <summary>
    /// Логика взаимодействия для Enter_window.xaml
    /// </summary>
    public partial class Enter_window : Window
 {
        StringBuilder connectionBuilder;

        public Enter_window() // Конструктор
        {
            InitializeComponent();
            connectionBuilder = new StringBuilder(ConfigurationManager.ConnectionStrings["MuseumDB"].ConnectionString);
            txtUName.Items.Add("Посетитель");
            txtUName.Items.Add("Практикант");
            txtUName.Items.Add("Сотрудник");
        }

        private void BtnEnter_Click(object sender, RoutedEventArgs e) // Вход с именем и паролем
        {
            string name = txtUName.Text;
            string pass = txtPass.Text;
            connectionBuilder.AppendFormat("Integrated Security=false;UID={0};PWD={1}", name, pass);
            BeginWork(connectionBuilder.ToString(),name);
            
        }

        private void btnAdmin_Click(object sender, RoutedEventArgs e) // Вход через УЗ Windows
        {
            connectionBuilder.Append("Integrated Security=True;");
            BeginWork(connectionBuilder.ToString(),"Admin");
        }

        private void BeginWork(string connectionString,string RoleName) // Создание подключения и инициализация рабочей формы
        {
            try
            {               
                SqlConnection SqlConnect = new SqlConnection(connectionString);                
                SqlConnect.Open();
                Employee user = new Employee(0);
                user.Name = RoleName;
                Work_Window work = new Work_Window(connectionBuilder.ToString(), user, RoleName); 
                switch (RoleName)
                {
                    case "Admin":
                        user.ID = 1;
                        user.FindSelf(SqlConnect);
                        work = new Work_Window(connectionBuilder.ToString(), user, RoleName);
                        break;
                    case "Посетитель":
                        work = new Work_Window(connectionBuilder.ToString(), user, RoleName);
                        break;
                    case "Практикант":
                        work = new Work_Window(connectionBuilder.ToString(), user, RoleName);
                        break;
                    case "Сотрудник":
                        work = new Work_Window(connectionBuilder.ToString(), user, RoleName);
                        break;
                    default:
                        throw new Exception("Неизвестная роль!");
                }
                SqlConnect.Close();
                work.Show();

                this.Close();
            }
            catch (SqlException err)
            {
                connectionBuilder = new StringBuilder(ConfigurationManager.ConnectionStrings["MuseumDB"].ConnectionString);
                MessageBox.Show(err.Message.ToString());
            }
            catch (Exception err)
            {
                connectionBuilder = new StringBuilder(ConfigurationManager.ConnectionStrings["MuseumDB"].ConnectionString);
                MessageBox.Show(err.Message.ToString());
            }
        }
    }
}
